let inner = document.querySelector(".inner");
let log = document.querySelector(".log");

function setCoords(e, type)
{
  let idX = type + "X";
  let idY = type + "Y";

  document.getElementById(idX).innerText = e[idX];
  document.getElementById(idY).innerText = e[idY];
}

function update(e)
{
  setCoords(e, "page");
}

function addImage(e)
{
	let idSpan = "span" + IDSpan;
  let idCore = "core" + IDSpan;

	sizeimg = getRandomIntInclusive(70,100);

    var cellule = document.createElement( 'span' );
	cellule.setAttribute("id", idSpan);
	cellule.setAttribute("class", "cellule");
	cellule.setAttribute("style",`top:${e.pageY}px;left:${e.pageX}px;position:fixed;width:${sizeimg}px;height:${sizeimg}px`);
	var content = document.getElementById( 'content');
	content.insertBefore( cellule , content.lastChild );
	sizeimg = sizeimg *0.3;
	var core = document.createElement( 'span' );
	core.setAttribute("id", idCore);
	core.setAttribute("class", "core");
	core.setAttribute("style",`width:${sizeimg}px;height:${sizeimg}px`);
	var content2 = document.getElementById( idSpan);
	content2.insertBefore( core , content2.lastChild );
  IDSpan++;

  /*let img = new Image();
  img.src = "cellule.png";
  img.setAttribute("id", "cellule");
  img.setAttribute("class", "cellule");
  img.setAttribute("style",`top:${e.pageY}px;left:${e.pageX}px;position:fixed;width:${sizeimg}px`);
  inner.appendChild(img);*/
  animateDiv();
}

function checkClick(e)
{
  if(e.which == 1 )
  {
    addImage(e);
  }
  else if(e.which == 2)
  {
    let classCell = document.getElementsByClassName("cellule");
    let i = 0 ;

    while(classCell.length > 0)
    {
      deleteAnnimation2(classCell[i]);
      i++;
      setTimeout(function() {
        classCell[0].parentNode.removeChild(classCell[0]);
    }, 3500);
    }
  }
  else if(e.which == 3)
  {
    let idMoinsUn = IDSpan-1;
    let idSpan = "span" + idMoinsUn;
    let idCore = "core" + idMoinsUn;
    console.log(idSpan);
    let el = document.getElementById(idSpan);
    deleteAnnimation(el);
    IDSpan--;
  }
}

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min +1)) + min;
}
function makeNewPosition(){

  // Get viewport dimensions (remove the dimension of the div)
  var h = $(window).height() - 50;
  var w = $(window).width() - 50;

  var nh = Math.floor(Math.random() * h);
  var nw = Math.floor(Math.random() * w);

  return [nh,nw];

}

function animateDiv(){
  var newq = makeNewPosition();
  var oldq = $('.cellule').offset();
  var speed = calcSpeed([oldq.top, oldq.left], newq);

  $('.cellule').animate({ top: newq[0], left: newq[1] }, speed, function(){
    animateDiv();
  });

};

function calcSpeed(prev, next) {

  var x = Math.abs(prev[1] - next[1]);
  var y = Math.abs(prev[0] - next[0]);

  var greatest = x > y ? x : y;

  var speedModifier = 0.05;
  var speed = Math.ceil(greatest/speedModifier);

  return speed;

}

function deleteAnnimation(el)
{
  el.classList.add("deleteAnnimation");
  setTimeout(function() {
  el.remove();
}, 3500);

}

function deleteAnnimation2(el)
{
  el.classList.add("deleteAnnimation");
}

inner.addEventListener("click", update, false);
inner.addEventListener("mousedown" , checkClick);

class getIdSpan {
  constructor(idSpan) {
    IDSpan = idSpan;
  }
}

let IDSpan = 0;
